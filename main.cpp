#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <psapi.h>
#pragma comment(lib, "psapi.lib")

#include <cstdio>
#include <sstream>

#include "d3d8/include/d3d8.h"
#include "d3d8/include/d3dx8tex.h"

#include "UTF8.h"

#define HOOK(offset, dstFunc) *(DWORD *)((DWORD)offset + 1) = (DWORD)dstFunc - ((DWORD)offset) - 5

#define DrawTextCall 0x12849E // Version 571C093D

int *v101EAF2C = nullptr;
int *v102AA178 = nullptr;

int _fastcall hookDrawText(int *_this, int ecx, char *string, int length, int x, int y, int color)
{
	//IDirect3DDevice8 *pD3DDevice = (IDirect3DDevice8 *)_this[0];
	IDirect3DTexture8 *texture = (IDirect3DTexture8 *)(_this[1]);
	ID3DXSprite *pSprite = (ID3DXSprite *)(_this[2]);
	int _length;
	if (texture && pSprite && (_length = length) != 0)
	{
		//static ID3DXFont* font = nullptr;
		//if (!font)
		{
			//if (pD3DDevice)
			{
				/*LOGFONT logFont = {};
				logFont.lfHeight = 18;
				logFont.lfWeight = FW_BOLD;
				logFont.lfQuality = ANTIALIASED_QUALITY;
				logFont.lfPitchAndFamily = FF_MODERN;
				logFont.lfCharSet = DEFAULT_CHARSET;
				strcpy_s(logFont.lfFaceName, "Verdana");
				D3DXCreateFontIndirect(pD3DDevice, &logFont, &font);*/
			}
		}

		if (length == -1)
			_length = strlen(string);

		std::wstring wstring = utf8_mbstowcs(string, _length);

		int charWidth = _this[4], charHeight = _this[3];
		int left = 0;
		wchar_t currentChar;
		for (size_t i = 0; i < wstring.length(); ++i)
		{
			currentChar = wstring[i];
			if (HIBYTE(currentChar) != 0)
			{
				int penx = left + x - 1;
				int peny = y - 1;
				RECT rc = { penx, peny, 100, 100 };
				//font->DrawTextW(&currentChar, 1, &rc, 0, color);
				left += (_this['A' + 7] * 2);
			}
			else
			{
				int charX = (currentChar & 0xF), charY = (currentChar >> 4);
				RECT rc = { charWidth * charX,
					charHeight * charY,
					charWidth * (charX + 1),
					charHeight * (charY + 1) };

				D3DXVECTOR2 translation((float)(left + x - 1), (float)(y - 1));

				pSprite->Begin();
				pSprite->Draw(texture, &rc, nullptr, nullptr, 0, &translation, color);
				pSprite->End();

				left += _this[currentChar + 7];
			}
		}

		return left;
	}
	return 0;
}

extern "C" BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		HMODULE hVCMPDll = GetModuleHandle("vcmp-game.dll");
		if (hVCMPDll)
		{
			MODULEINFO modInfo;
			if (GetModuleInformation(GetCurrentProcess(), hVCMPDll, &modInfo, sizeof(modInfo)))
			{
				std::stringstream ss;
				ss << "vcmp-game.dll Base:" << modInfo.lpBaseOfDll << " Size:" << modInfo.SizeOfImage;
				MessageBox(0, ss.str().c_str(), "", 0);

				v101EAF2C = (int*)((int)modInfo.lpBaseOfDll + 0x1EAF2C);
				v102AA178 = (int*)((int)modInfo.lpBaseOfDll + 0x2AA178);

				DWORD memProtect;
				if (VirtualProtect((LPVOID)((DWORD)modInfo.lpBaseOfDll + DrawTextCall), 5, PAGE_EXECUTE_READWRITE, &memProtect))
				{
					HOOK((DWORD)modInfo.lpBaseOfDll + DrawTextCall, hookDrawText);
					VirtualProtect((LPVOID)((DWORD)modInfo.lpBaseOfDll + DrawTextCall), 5, memProtect, &memProtect);
				}

				DisableThreadLibraryCalls(hModule);
			}
		}
		break;
	}
	}
	return TRUE;
}
